<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'produtos';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['descricao', 'programa_id', 'cod_barras', 'cod_itec', 'forma_desconto', 'orientacoes'];

    /**
     * Define an inverse one-to-one or many relationship.
     */
    public function programa()
    {
        return $this->belongsTo('App\Programa');
    }
}
