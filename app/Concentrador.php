<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Concentrador extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'concentradores';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['descricao', 'anexo'];
}
