<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Produto;
use App\Programa;
use App\CodigoBarra;

class ProdutosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produtos = Produto::all();

        return view('produtos.index', compact('produtos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        return view('produtos.create', [

        ])
            ->with('programas', Programa::pluck('descricao', 'id')->toArray());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'descricao' => 'required|unique:produtos,descricao',
            'cod_itec' => 'required',
            'programa_id' => 'required'
        ]);

        $produto = Produto::create($request->all());

        CodigoBarra::where('produto_id', $produto->id)->delete();

        $codigos_barras = explode("\n", $request->codigos_barras);
        foreach($codigos_barras as $cod_barras)
        {
            if ($cod_barras)
            {
                CodigoBarra::create([
                    'produto_id' => $produto->id,
                    'cod_barras' => trim($cod_barras)
                ]);
            }
        }

        if ($request->bt_continuar)
            return redirect('admin/produtos/create')->with('success', 'Produto incluído com sucesso.');

        return redirect('admin/produtos')->with('success', 'Produto incluído com sucesso.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produto = Produto::findOrFail($id);

        $produto->codigos_barras = $this->getCodigoBarras($produto);

        return view('produtos.edit', compact('produto'))
            ->with('programas', Programa::pluck('descricao', 'id')->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'descricao' => 'required|unique:produtos,descricao,'.$id,
            'cod_itec' => 'required',
            'programa_id' => 'required'
        ]);

        $produto = Produto::findOrFail($id);

        $produto->update($request->all());

        CodigoBarra::where('produto_id', $produto->id)->delete();

        $codigos_barras = explode("\n", $request->codigos_barras);
        foreach($codigos_barras as $cod_barras)
        {
            if ($cod_barras)
            {
                CodigoBarra::create([
                    'produto_id' => $produto->id,
                    'cod_barras' => trim($cod_barras)
                ]);
            }
        }

        return redirect('admin/produtos')->with('success', 'Produto atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            Produto::destroy($id);
        }
        catch (\Exception $e)
        {
            return redirect('concentradores')
                ->with('error', 'Não foi possível excluir este registro. '
                    . 'Ele está sendo usado em outro cadastro. '
                    . 'Remova todas as referências a este registro e tente novamente.');
        }

        return redirect('admin/produtos')->with('success', 'Produto excluído com sucesso.');
    }


    private function getCodigoBarras($produto)
    {
        $codigos_barras =  \DB::select("select cod_barras from codigos_barras where produto_id = ".$produto->id);

        $result = [];
        foreach($codigos_barras as $cod_barras)
        {
            $result[] = $cod_barras->cod_barras;
        }

        return implode("\n", $result);
    }

}

