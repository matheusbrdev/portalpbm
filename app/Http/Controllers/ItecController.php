<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Response;
use App\Produto;

class ItecController extends Controller
{

    /*
    SELECT CD_PROD, DS_PROD, CD_BARRA
	FROM ITECSERVER.IndianaProducao.dbo.V_EST_PROD
    */

    public function autocomplete(Request $request)
    {
        if (strlen($request->q) >= 5)
        {
            $produtos = \DB::connection('210')->select("SELECT CD_PROD as data, DS_PROD as value
                FROM EST_PROD
                WHERE DS_PROD LIKE '".$request->q."%'");

            return Response::json([
                'suggestions' => $produtos
            ]);
        }

        return Response::json([]);
    }

    public function produto(Request $request)
    {
        $produto = \DB::connection('210')->select("SELECT CD_PROD, DS_PROD
            FROM EST_PROD
            WHERE CD_PROD = ".$request->id."");

        $produto = ($produto) ? $produto[0] : null;

        if ($produto)
        {
            $produto->codigos_barras = $this->getCodigoBarras($produto);
        }

        return Response::json($produto);
    }

    public function produtoPorCodbarras(Request $request)
    {
        $produto = \DB::connection('210')->select("SELECT CD_PROD, DS_PROD
            FROM V_EST_PROD
            WHERE CD_BARRA = '".$request->id."'");

        $produto = ($produto) ? $produto[0] : null;

        if ($produto)
        {
            $produto->codigos_barras = $this->getCodigoBarras($produto);
        }

        return Response::json($produto);
    }

    public function produtoPorCoditec(Request $request)
    {
        $produto = \DB::connection('210')->select("SELECT CD_PROD, DS_PROD
            FROM EST_PROD
            WHERE CD_PROD = ".$request->id."");

        $produto = ($produto) ? $produto[0] : null;

        if ($produto)
        {
            $produto->codigos_barras = $this->getCodigoBarras($produto);
        }

        return Response::json($produto);
    }

    private function getCodigoBarras($produto)
    {
        $codigos_barras =  \DB::connection('210')->select("SELECT CD_BARRA FROM
            EST_PROD_CD_BARRA WHERE CD_PROD = ".$produto->CD_PROD);

        $result = [];
        foreach($codigos_barras as $cod_barras)
        {
            $result[] = $cod_barras->CD_BARRA;
        }

        return implode("\n", $result);
    }
}

