<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Programa;
use App\Concentrador;

class ProgramasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programas = Programa::all();

        return view('programas.index', compact('programas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('programas.create')
            ->with('concentradores', Concentrador::pluck('descricao', 'id')->toArray());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'descricao' => 'required|unique:programas,descricao',
            'concentrador_id' => 'required'
        ]);

        Programa::create($request->all());

        return redirect('admin/programas')->with('success', 'Programa incluído com sucesso.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $programa = Programa::findOrFail($id);

        return view('programas.edit', compact('programa'))
            ->with('concentradores', Concentrador::pluck('descricao', 'id')->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'descricao' => 'required|unique:programas,descricao,'.$id,
            'concentrador_id' => 'required'
        ]);

        $programa = Programa::findOrFail($id);

        $programa->update($request->all());

        return redirect('admin/programas')->with('success', 'Programa atualizado com sucesso.');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            Programa::destroy($id);
        }
        catch (\Exception $e)
        {
            return redirect('concentradores')
                ->with('error', 'Não foi possível excluir este registro. '
                    . 'Ele está sendo usado em outro cadastro. '
                    . 'Remova todas as referências a este registro e tente novamente.');
        }

        return redirect('admin/programas')->with('success', 'Programa excluído com sucesso.');
    }
}

