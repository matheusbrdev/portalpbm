<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Concentrador;

class ConcentradoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $concentradores = Concentrador::all();

        return view('concentradores.index', compact('concentradores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('concentradores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'descricao' => 'required|unique:concentradores,descricao'
        ]);

        $input = $request->all();

        // upload de anexo
        //
        $upload = $this->uploadAnexo($request);

        if ($upload) {
            $input['anexo'] = $upload;
        }

        Concentrador::create($input);

        return redirect('admin/concentradores')->with('success', 'Concentrador incluído com sucesso.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $concentrador = Concentrador::findOrFail($id);

        return view('concentradores.edit', compact('concentrador'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'descricao' => 'required|unique:concentradores,descricao,'.$id
        ]);

        $concentrador = Concentrador::findOrFail($id);

        $input = $request->all();

        // excluir anexo
        //
        if ($request->excluir_anexo)
        {
            $path = public_path() . '/uploads/' . $concentrador->anexo;
            \File::delete($path);
            $input['anexo'] = null;
        }

        // upload de anexo
        //
        $upload = $this->uploadAnexo($request);

        if ($upload) {
            $input['anexo'] = $upload;
        }

        $concentrador->update($input);

        return redirect('admin/concentradores')->with('success', 'Concentrador atualizado com sucesso.');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            Concentrador::destroy($id);
        }
        catch (\Exception $e)
        {
            return redirect('concentradores')
                ->with('error', 'Não foi possível excluir este registro. '
                    . 'Ele está sendo usado em outro cadastro. '
                    . 'Remova todas as referências a este registro e tente novamente.');
        }

        return redirect('admin/concentradores')->with('success', 'Concentrador excluído com sucesso.');
    }


    private function uploadAnexo(Request $request)
    {
        $anexo = null;

        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $extensaoArquivo = strtolower(\File::extension($file->getClientOriginalName()));
            $nomeArquivo = $file->getClientOriginalName();
            $nomeArquivo = 'concentrador_'.md5($nomeArquivo.date('YmdHis')).'.'.$extensaoArquivo;
            $file->move(public_path() . '/uploads/', $nomeArquivo);
            $anexo = $nomeArquivo;
        }

        return $anexo;
    }

}
