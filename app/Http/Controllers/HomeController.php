<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Produto;
use App\CodigoBarra;

class HomeController extends Controller
{

    public function index()
    {
        return view('welcome');
    }


    public function autocomplete(Request $request)
    {
        if (strlen($request->q) >= 3) {
            $produtos = Produto::select('descricao as value', 'id as data')
                ->where('descricao', 'like', '%' . $request->q . '%')
                ->get();

            return Response::json([
                'suggestions' => $produtos->toArray()
            ]);
        }

        return Response::json([]);
    }


    public function produto(Request $request)
    {
        $produto = Produto::find($request->id);

        return view('produto', compact('produto'));
    }


    public function produtoPorCodbarras(Request $request)
    {
        $codigo_barras = CodigoBarra::where('cod_barras', '=', $request->id)->first();

        if (!$codigo_barras)
            return "";

        $produto = Produto::find($codigo_barras->produto_id);

        return view('produto', compact('produto'));
    }


    public function produtoPorCoditec(Request $request)
    {
        $produto = Produto::where('cod_itec', '=', $request->id)->first();

        if (!$produto)
            return "";

        $produto = Produto::find($produto->id);

        return view('produto', compact('produto'));
    }

}
