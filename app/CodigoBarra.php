<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodigoBarra extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'codigos_barras';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['produto_id', 'cod_barras'];
}
