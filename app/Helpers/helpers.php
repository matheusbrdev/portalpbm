<?php
/**
 * Acrescenta um item em branco no começo do array
 * para uma boa apresentação dos campos do tipo 'combobox
 *
 * @param array
 * @return array
 * @author leonardo@consept.com.br
 */
function formSelectArray($array)
{
    return ['' => ''] + $array;
}

/**
 * Transforma uma data do formato americano para o
 * formato brasileiro
 *
 * @return string
 * @author leonardo@consept.com.br
 */
function date_br($date)
{
    if (!$date) {
        return '';
    }

    return implode('/', array_reverse(explode('-', $date)));
}

/**
 * Transforma um datetime do formato americano para o
 * formato brasileiro
 *
 * @return string
 * @author leonardo@consept.com.br
 */

if (! function_exists('datetime_br')) {
    function datetime_br($datetime)
    {
        if (!$datetime) {
            return '';
        }

        $date = substr($datetime, 0, 10);
        $time = substr($datetime, 11, 8);

        return implode('/', array_reverse(explode('-', $date))) . ' ' . $time;
    }
}


?>
