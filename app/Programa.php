<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programa extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'programas';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['descricao', 'concentrador_id'];

    /**
     * Define an inverse one-to-one or many relationship.
     */
    public function concentrador()
    {
        return $this->belongsTo('App\Concentrador');
    }
}
