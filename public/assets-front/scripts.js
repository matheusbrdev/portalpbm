
jQuery(document).ready(function() {

	$('#btn-continuar').click(function(e) {

		if( $("#cpf_cnpj").val() == "" ) 
		{
			e.preventDefault();
			swal("É necessário informar o CPF/CNPJ para continuar.");
			return false;
		}

		$.get( APP_URL + '/consulta-empresa/' +  $('#cpf_cnpj').val())
  			.done(function( data ) {

				if (data.error) {  
					swal('' + data.msg + '');
					return false;
				}

				if (data.empresa)
				{
					$('#razao_social').val(data.empresa.razao_social);
					$('#razao_social').prop('readonly', true);
				}
				
				$('.step1').hide();
				$('#cpf_cnpj').prop('readonly', true);
				$('.step2').show();

			})
			.fail(function() {
				swal("Ocorreu um erro desconhecido! Entre em contato com nosso suporte técnico.")
			});

			//alert(APP_URL);
	});


	$("#cpf_cnpj").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    /*
        Registration form validation
    */
    $('.registration-form').on('submit', function(e) {
    	
    	$(this).find('input[type="text"], input[type="password"]').each(function() {
    		if( $(this).val() == "" ) 
			{
    			e.preventDefault();
    			swal("Favor preencher todos os campos para efetuar o cadastro.");
				return false;
    		}
    	});


    });
    
	$('.registration-form').on('keypress', function(e) {
		var keyCode = e.keyCode || e.which;
		if (keyCode === 13) { 
			e.preventDefault();
			return false;
		}
	});

    
});
