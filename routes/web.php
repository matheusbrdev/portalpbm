<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/autocomplete', 'HomeController@autocomplete');
Route::get('/produto', 'HomeController@produto');
Route::get('/produto-por-codbarras', 'HomeController@produtoPorCodbarras');
Route::get('/produto-por-coditec', 'HomeController@produtoPorCoditec');

Route::auth();
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

    Route::get('/', 'ProdutosController@index');
    Route::resource('produtos', 'ProdutosController');
    Route::resource('concentradores', 'ConcentradoresController');
    Route::resource('programas', 'ProgramasController');

    Route::get('/itec-autocomplete', 'ItecController@autocomplete');
    Route::get('/itec-produto', 'ItecController@produto');
    Route::get('/itec-produto-por-codbarras', 'ItecController@produtoPorCodbarras');
    Route::get('/itec-produto-por-coditec', 'ItecController@produtoPorCoditec');

});
