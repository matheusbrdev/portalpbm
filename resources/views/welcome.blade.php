@extends('layouts.application')

@section('content')

<style type="text/css">

</style>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading">Pesquisar</div>

                <div class="panel-body">

                    <div class="radio radio-info radio-inline">
                        <input type="radio" id="inlineRadio1" value="descricao" name="t" checked>
                        <label for="inlineRadio1"> Nome do Produto </label>
                    </div>

                    <div class="radio radio-info radio-inline">
                        <input type="radio" id="inlineRadio2" value="cod_barras" name="t">
                        <label for="inlineRadio2"> Código de Barras </label>
                    </div>

                    <div class="radio radio-info radio-inline">
                        <input type="radio" id="inlineRadio3" value="cod_itec" name="t">
                        <label for="inlineRadio3"> Código Itec </label>
                    </div>

                    <div class="form-group" style="margin-top:15px">
                        <input type="text" name="q" id="q" class="form-control" style="background-color: white">
                    </div>

                    <div id="status"></div>

                </div>

            </div>
        </div>
    </div>
</div>

 <div class="row"> <!-- Inicio da Linha da Classe Row -->
    <div class="col-md-12">
      <div align="center" class="loader">
          <i class="fa fa-spin fa-circle-o-notch fa-3x" style="color:#3BAFDA"></i> <br> <br>
      </div>
    </div>
</div>

<div id="resultado"></div>


<script>
@section('jquery.ready')

    $("input[type=radio]").click(function() {
        var selected = $( "input[type=radio]:checked" ).val();

        $( "#q" ).val("");
        $( "#resultado" ).html("");

        // pesquisa por descrição: habilita o autocomplete
        if (selected == 'descricao') {
            $( "#q" ).off("keydown"); // dsabilita onlyNumbers()
            $( "#q" ).unbind("keypress"); // desabilita evento keypress()
            $( "#q" ).autocomplete({
                serviceUrl: '{{ url("/autocomplete") }}',
                paramName: 'q',
                dataType: 'json',
                showNoSuggestionNotice: true,
                noSuggestionNotice: 'Desculpe, nenhum resultado encontrado.',
                onSelect: function(suggestion) {
                    //$( "#status" ).html('Selecionado: ' + suggestion.value + ', ' + suggestion.data);
                    produto(suggestion.data);
                }
            });
        }
        // pesquisa por código de barras ou código itec: desabilita o autocomplete
        else {
            $( "#q" ).autocomplete().clear();
            $( "#q" ).autocomplete().dispose();
            $( "#q" ).onlyNumbers();

            $( '#q' ).keypress(function(e) {
                if(e.which == 13) {
                    produto($( "#q" ).val());
                }
            });
        }

    })


    // inicializa a pesquisa padrão
    $( "input[type=radio]:checked" ).trigger( "click" );


    function produto(id)
    {
        var selected = $( "input[type=radio]:checked" ).val();

        var url = "/produto";

        if (selected == 'cod_barras')
            url = "/produto-por-codbarras";
        else if (selected == 'cod_itec')
            url = "/produto-por-coditec";

        var request = $.ajax({
          url: '{{ url("/") }}' + url,
          method: "GET",
          data: { id : id },
          dataType: "html",
          beforeSend: function(){
            $( "#resultado" ).html("");
            $('.loader').show();
          },
          complete: function(){
            $('.loader').hide();
          },
        });

        request.done(function( msg ) {

            if (msg) {
                $( "#resultado" ).html( msg );
            } else {
                alert("Produto não encontrado.");
            }

        });



    }


@endsection
</script>
@endsection
