@extends('layouts.application')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Editando concentrador...</h4>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">

            {!! Form::model($concentrador, ['url'=>'admin/concentradores/'.$concentrador->id, 'method'=>'PATCH', 'files'=>true]) !!}

                @include('concentradores.form')

            {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection