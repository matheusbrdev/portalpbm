
<div class="row">
    <div class="col-md-6">
        
        <div class="form-group">
            {!! Form::label('descricao', 'Descrição') !!}
            {!! Form::text('descricao', null, ['class'=>'form-control']) !!}
        </div>


        <div class="form-group required">
            {!! Form::label('anexo', 'Imagem') !!}
            <div>
                {!! Form::hidden('excluir_anexo', 0, ['id'=>'excluir_anexo']) !!}

                @if (isset($concentrador->anexo) and ($concentrador->anexo))
                    <div id="div-anexo" style="margin-top:7px">
                        <a id="link-fullscreen" target="_blank" href="{{ url('uploads/'.$concentrador->anexo) }}" style="color:#00b19d;">
                            <!--<i class="glyphicon glyphicon-fullscreen"></i>-->
                            <img src="{{ url('uploads/'.$concentrador->anexo) }}" width="120">
                        </a>

                        &nbsp;

                        <a id="link-remove" href="#" style="color:#c91010">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>              
                    </div>
                    {!! Form::file('file', ['class'=>'', 'id'=>'file', 'style'=>'display:none']) !!}
                @else
                    {!! Form::file('file', ['class'=>'', 'id'=>'file']) !!}
                @endif            
            </div>
        </div>

        <div class="form-group">
            <input type="submit" value="Salvar" class="btn btn-primary">
            <a href="{{ url('admin/concentradores') }}" class="btn btn-default">Cancelar</a>
        </div>

    </div>
</div>

<script>  
@section('jquery.ready')

    $('#link-remove').click(function(event) {
        event.preventDefault();
        $('#div-anexo').hide();
        $('#file').show();
        $('#excluir_anexo').val('1');
    });
    
@stop
</script>