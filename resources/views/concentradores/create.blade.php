@extends('layouts.application')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Novo concentrador...</h4>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">

            {!! Form::open(['url' => 'admin/concentradores', 'files'=>true]) !!}
    
                @include('concentradores.form')
                
            {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection