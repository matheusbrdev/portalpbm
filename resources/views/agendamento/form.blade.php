@extends('layouts.app')

@section('content')

<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Solicitação de Agendamento</h4>
    </div>
</div>
<!-- Page-Title -->

    <style>
        .form-control { text-transform: uppercase }
    </style>

    {!! Form::open(['url'=>'agendamentos/solicitacao', 'files'=>true, 'id'=>'form-solicitacao', 'class'=>'form-horizontal', 'style'=>'']) !!}

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Dados do Pedido / Fornecedor</h3>
                </div>
                <div class="panel-body">


                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="nro_pedido" class="col-sm-3 control-label">Nº do Pedido</label>
                                <div class="col-sm-5">
                                    {!! Form::text('nro_pedido', null, ['class'=>'form-control', 'id'=>'nro_pedido', 'placeholder'=>'Somente números']) !!}
                                </div>
                            </div>

                            <div class="form-group div-pedido" style="display:none">
                                <label for="dt_emissao" class="col-sm-3 control-label">Data de Emissão</label>
                                <div class="col-sm-5">
                                    {!! Form::text('dt_emissao', null, ['class'=>'form-control', 'id'=>'dt_emissao']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="cpf_cnpj" class="col-sm-3 control-label">CPF/CNPJ</label>
                                <div class="col-sm-5">
                                    {!! Form::text('cpf_cnpj', null, ['class'=>'form-control', 'id'=>'cpf_cnpj', 'placeholder'=>'Somente números']) !!}
                                </div>
                            </div>

                            <div class="form-group div-pedido" style="display:none">
                                <label for="razao_social" class="col-sm-3 control-label">Razão Social</label>
                                <div class="col-sm-9">
                                    {!! Form::text('razao_social', null, ['class'=>'form-control', 'id'=>'razao_social']) !!}
                                </div>
                            </div>

                            <div class="form-group step1">
                                <label class="col-sm-3 control-label"></label>
                                <div class="col-sm-9">
                                    <div class="">
                                        <input type="button" id="bt-continuar1" value="Continuar" class="btn btn-primary">
                                        <a href="{{ url('/') }}" class="btn btn-default">Cancelar</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>            
 
    <div class="row div-transportadora" style="display:none">
        <div class="col-sm-12">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Dados da Transportadora</h3>
                </div>
                <div class="panel-body">

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="trans_cpf_cnpj" class="col-sm-3 control-label">CPF/CNPJ</label>
                                <div class="col-sm-5">
                                    {!! Form::text('trans_cpf_cnpj', null, ['class'=>'form-control', 'id'=>'trans_cpf_cnpj', 'placeholder'=>'Somente números']) !!}
                                </div>
                            </div>

                            <div class="form-group div-solicitacao" style="display:none">
                                <label for="trans_razao_social" class="col-sm-3 control-label">Razão Social</label>
                                <div class="col-sm-9">
                                    {!! Form::text('trans_razao_social', null, ['class'=>'form-control', 'id'=>'trans_razao_social']) !!}
                                </div>
                            </div>          

                            <div class="form-group step2">
                                <label class="col-sm-3 control-label"></label>
                                <div class="col-sm-9">
                                    <div class="">
                                        <input type="button" id="bt-continuar2" value="Continuar" class="btn btn-primary">
                                        <a href="{{ url('/') }}" class="btn btn-default">Cancelar</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>   

    <div class="row div-solicitacao" style="display:none">
        <div class="col-sm-12">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Notas Fiscais</h3>
                </div>
                <div class="panel-body">

                    <div class="row">

                        <div class="col-md-10">

                            <div class="form-group">
                                <label for="trans_cpf_cnpj" class="col-sm-1 control-label"></label>    
                                <div class="col-sm-10">      

                                    <table class="table" width="80%" id="table-nf">
                                        <thead>
                                            <tr>
                                                <th width="15%">Nro NF</th>
                                                <th width="15%">Emissão</th>
                                                <th width="20%">XML</th>
                                                <th width="20%">DANFE</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                              <!-- notas fiscais -->      
                                        </tbody>
                                    </table>

                                    <div style="padding: 8px;">
                                        <input type="button" class="btn btn-primary" value="incluir" id="add_row">
                                    </div>                             

                                </div>
                            </div>                            

                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>   

    <div class="row div-solicitacao" style="display:none">
        <div class="col-sm-12">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Detalhes da Solicitação</h3>
                </div>
                <div class="panel-body">

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="tipo_solicitacao_id" class="col-sm-3 control-label">Tipo de Solicitação</label>
                                <div class="col-sm-9">
                                    {!! Form::select('tipo_solicitacao_id', formSelectArray($tipos_solicitacao)
                                        , null,['class'=>'form-control', 'id'=>'tipo_solicitacao_id']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="tipo_veiculo_id" class="col-sm-3 control-label">Tipo de Veículo</label>
                                <div class="col-sm-9">
                                    {!! Form::select('tipo_veiculo_id', formSelectArray($tipos_veiculo), null,['class'=>'form-control', 'id'=>'tipo_veiculo_id']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="tipo_produto_id" class="col-sm-3 control-label">Tipo de Produto</label>
                                <div class="col-sm-9">
                                    {!! Form::select('tipo_produto_id', formSelectArray($tipos_produto), null,['class'=>'form-control', 'id'=>'tipo_produto_id']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="qtde_volumes" class="col-sm-3 control-label">Qtde. Volumes</label>
                                <div class="col-sm-5">
                                    {!! Form::text('qtde_volumes', null, ['class'=>'form-control', 'id'=>'qtde_volumes']) !!}
                                </div>
                            </div>

                            <div class="form-group div-datepicker-inline" style="display:none">
                                <label for="dt_solicitada" class="col-sm-3 control-label">Previão de Entrega</label>
                                <div class="col-sm-5">

                                    {!! Form::hidden('dt_solicitada', null, ['id'=>'dt_solicitada']) !!}

                                    <div class="input-group">
                                        <div id="datepicker-inline"></div>
                                    </div><!-- input-group -->
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="forma_alocacao_id" class="col-sm-3 control-label">Forma de Alocação</label>
                                <div class="col-sm-9">
                                    {!! Form::select('forma_alocacao_id', formSelectArray($formas_alocacao), null,['class'=>'form-control', 'id'=>'forma_alocacao_id']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="paga_descarga" class="col-sm-3 control-label">Pagar Descarga?</label>
                                <div class="col-sm-5">
                                    {!! Form::select('paga_descarga', [''=>'', '1'=>'Sim', '0'=>'Não'], null,['class'=>'form-control', 'id'=>'paga_descarga']) !!}
                                </div>
                            </div>

                            <div class="form-group div_valor_descarga" style="display:none">
                                <label for="valor_descarga" class="col-sm-3 control-label">Valor da Descarga</label>
                                <div class="col-sm-5">
                                    {!! Form::text('valor_descarga', 0.00, ['class'=>'form-control', 'id'=>'valor_descarga', 'style'=>'border:none']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="forma_devolucao_id" class="col-sm-3 control-label">Tipo de Devolução Aceitável</label>
                                <div class="col-sm-9">
                                    {!! Form::select('forma_devolucao_id', formSelectArray($formas_devolucao), null,['class'=>'form-control', 'id'=>'forma_devolucao_id']) !!}
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="sugestao" class="col-sm-3 control-label">Sugestão / Reclamação / Observação</label>
                                <div class="col-sm-9">
                                    {!! Form::textarea('sugestao', '',['class'=>'form-control', 'id'=>'sugestao', 'style'=>'height:100px']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label"> </label>
                                <div class="col-sm-9">
                                     
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label"></label>
                                <div class="col-sm-9">
                                    <div class="">
                                        <input type="submit" id="bt-salvar" value="Enviar Solicitação" class="btn btn-primary">
                                        <a id="bt-cancelar" href="{{ url('/') }}" class="btn btn-default">Cancelar</a>
                                    </div>
                                </div>
                            </div>                
             
                        </div>

                    </div>

                </div>

            </div>
        </div>


    </div>  
    {!! Form::close() !!}






@endsection

<script>
@section('jquery.ready')

	var baseDatesDisabled = [
    @foreach($bloqueios as $bloqueio)
        '{{ implode('/', array_reverse(explode('-', $bloqueio->data))) }}'
        @if ($bloqueio != $bloqueios->last()) 
            {{ ',' }}
        @endif
    @endforeach
    ];

    $('#datepicker-inline').datepicker({
        format:"dd/mm/yyyy", 
        language:"pt-BR",
        startDate: "{{ date('d/m/Y') }}",
        datesDisabled: baseDatesDisabled
    });
    
    $('#datepicker-inline').on("changeDate", function() {
        $('#dt_solicitada').val(
            $('#datepicker-inline').datepicker('getFormattedDate')
        );
    });

    $("#nro_pedido").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("#cpf_cnpj").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("#trans_cpf_cnpj").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("#qtde_volumes").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $('#bt-continuar1').click(function() {

        /**
         * nro do pedido
         */
        var nro_pedido = $('#nro_pedido').val();

        /**
         * cpf/cnpj do fornecedor vinculado ao pedido
         */
        var cpf_cnpj = $('#cpf_cnpj').val();

        /**
         * usuário informou o nro do pedido?
         */
        if (nro_pedido.length == 0)
        {
            //swal('Informe o nro do pedido.');
            swal('Informe o nro do pedido.');
            return ;
        }

        /**
         * usuário informou o cpf/cnpj do fornecedor?
         */
        if (cpf_cnpj.length == 0)
        {
            swal('Informe o CPF/CNPJ do fornecedor.');
            return ;
        }

        /**
         * Altera label do botão 'continuar' para 'aguarde'
         * e desabilita-o para evitar que o usuário clique mais de uma vez
         */
        var btn_legenda = $('#bt-continuar1').val();
        $('#bt-continuar1').val('Consultando! Aguarde...');
        $('#bt-continuar1').prop('disabled', true);

        /** 
         * executa consulta de pedidos via ajax
         * populando os campos referentes ao pedido
         */
        $.get("{{ url('agendamentos/pedido') }}/" + nro_pedido + "/" + cpf_cnpj)
            .done(function( data ) {
                
                /**
                 * pedido não encontrado... 
                 * habilita novamente o botão enviar e volta a legenda anterior
                 */
                if (data.error) {
                    $('#bt-continuar1').prop('disabled', false);
                    $('#bt-continuar1').val(btn_legenda);
                    
                    swal('' + data.msg + '');

                    return ;
                }
         
                /**
                 * Libera os campos ocultos para edição...
                 */
                $('.step1').hide();
                $('#nro_pedido').prop('readonly', true);
                $('#dt_emissao').prop('readonly', true);
                $('#cpf_cnpj').prop('readonly', true);
                $('#razao_social').prop('readonly', true);
                $('#dt_emissao').val(data.pedido.DT_EMI);
                $('#cpf_cnpj').val(data.pedido.CGC_CPF);
                $('#razao_social').val(data.pedido.RZ_FORN);
                $('.div-pedido').show();
                $('.div-transportadora').show();
                $('#trans_cpf_cnpj').focus();
                ativaConfirmacao();
            })
            .fail(function() {
                /**
                 * A requisição falhou...
                 * Habilita novamente o botão, voltando a lengenda anterior
                 */
                $('#bt-continuar1').prop('disabled', false);
                $('#bt-continuar1').val(btn_legenda);
                    
                swal("Erro desconhecido! Por favor, tente novamente.");
            });
    });

    /**
     * confirma os dados da transportadora
     * e libera os outros campos para edição
     */
    $('#bt-continuar2').click(function() {

        /**
         * cpf/cnpj da transportadora 
         */
        var cpf_cnpj = $('#trans_cpf_cnpj').val();

        /**
         * usuário informou o cpf/cnpj da transportadora?
         */
        if (cpf_cnpj.length == 0)
        {
            swal('Informe o CPF/CNPJ da transportadora.');
            return ;
        }

        /**
         * Altera label do botão 'continuar' para 'aguarde'
         * e desabilita-o para evitar que o usuário clique mais de uma vez
         */
        var btn_legenda = $('#bt-continuar2').val();
        $('#bt-continuar2').val('Consultando! Aguarde...');
        $('#bt-continuar2').prop('disabled', true);

        /** 
         * executa consulta de pedidos via ajax
         * populando os campos referentes ao pedido
         */
        $.get("{{ url('agendamentos/transportadora') }}/" + cpf_cnpj)
            .done(function( data ) {

                /**
                 * pedido não encontrado... 
                 * habilita novamente o botão enviar e volta a legenda anterior
                 */
                if (data.error) {
                    $('#bt-continuar2').prop('disabled', false);
                    $('#bt-continuar2').val(btn_legenda);
                    
                    if (data.field)
                    {
                        swal('' + data.msg + '');

                        return ;
                    }
                    
                    swal({
                        title: "Transportadora não encontrada! Gostaria de cadastra-la?",
                        showCancelButton: true,
                        cancelButtonClass: 'btn-default waves-effect waves-light',
                        confirmButtonClass: 'btn-primary waves-effect waves-light',
                        confirmButtonText: "Sim",
                        cancelButtonText: "Não",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    }, function (isConfirm) {
                        if (isConfirm) 
                        {
                            /**
                             * Libera os campos ocultos para edição...
                             */
                            $('.step2').hide();
                            $('#trans_cpf_cnpj').prop('readonly', true);
                            $('.div-solicitacao').show();
                        } 
                    });
                }
                else {
                    /**
                     * Exibe a razão social da transportadora
                     * e libera os campos ocultos para edição...
                     */
                    $('.step2').hide();
                    $('#trans_cpf_cnpj').prop('readonly', true);
                    $('#trans_razao_social').val(data.transportadora.razao_social);
                    $('#trans_razao_social').prop('readonly', true);
                    $('.div-solicitacao').show();                    
                }


            })
            .fail(function() {
                /**
                 * A requisição falhou...
                 * Habilita novamente o botão, voltando a lengenda anterior
                 */
                $('#bt-continuar2').prop('disabled', false);
                $('#bt-continuar2').val(btn_legenda);
                    
                swal("Erro desconhecido! Por favor, tente novamente.");
            });
    });

    /**
     * Desabilita o ENTER para envio de formulário
     */
	$('#form-solicitacao').on('keypress', function(e) {
		var keyCode = e.keyCode || e.which;
		if (keyCode === 13) { 
			e.preventDefault();
			return false;
		}
	});

    /** 
     * Envio do formulário de solicitação
     */
    $('#form-solicitacao').submit(function(e) {
        
        e.preventDefault();

        var form = $(this);

        url = form.attr('action');

        /**
         * Altera label do botão 'continuar' para 'aguarde'
         * e desabilita-o para evitar que o usuário clique mais de uma vez
         */
        var btn_legenda = $('#bt-salvar').val();
        $('#bt-salvar').val('Consultando! Aguarde...');
        $('#bt-salvar').prop('disabled', true);

        /**
         * Envia o formulário
         */
        //var posting = $.post( url, form.serialize(), contentType: 'multipart/form-data');

        $.ajax({
            url: url,
            cache: false,
            data: new FormData(this),
            contentType: false,
            processData: false,
            type: 'POST'
        })
        .done(function( data ) {
            
            if (data.error) // erro de validação
            {
                $('#bt-salvar').prop('disabled', false);
                $('#bt-salvar').val(btn_legenda);
                
                swal('' + data.msg  + '');
            }       
            else // sucesso
            {
                //Desabilita a confirmação antes do redirecionamento
                desativaConfirmacao();

                swal({   
                    title: "" + data.msg + "",   
                    text: "",   
                    type: "success",   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "OK!",   
                    closeOnConfirm: true 
                }, function(){   
                    window.location.href = "{{ url('/') }}";
                });
            }
        })
        .fail(function( data ) {
                $('#bt-salvar').prop('disabled', false);
                $('#bt-salvar').val(btn_legenda);

                swal("Erro desconhecido! Por favor, tente novamente.");
        });    

   });

    /**
     * Cancelar operação
     */
    $('#bt-cancelar').click(function() {
        desativaConfirmacao();
    });

    /**
     * Ativa confirmação, caso o usuário tente sair da página
     */
    function ativaConfirmacao()
    {
        window.onbeforeunload = confirmExit;
    }

    /**
     * Desativa confirmação, para que o usuário possa sair da página
     * sem ser questionado
     */
    function desativaConfirmacao()
    {
        window.onbeforeunload = null;
    }

    /**
     * Mensagem de confirmação, ao tentar sair da página - 
     * se confirmação estiver ativa no momento
     */
    function confirmExit()
    {
        return "Esta página quer saber se você deseja mesmo sair - informações fornecidas podem ser perdidas. Deseja mesmo sair desta página?";
    }


    $("#add").click(function() {
        $('#table-nf tbody>tr:last').clone(true).insertAfter('#table-nf tbody>tr:last');
        return false;
    });

    var nf_index = 0;

    $('#add_row').click(function() {
        var tr  = '<tr class="tr-nf-'+nf_index+'">'
                + '  <td><input type="text" name="nf['+nf_index+'][nro]" id="nf_nro_'+nf_index+'" class="form-control"></td>'
                + '  <td><input type="text" name="nf['+nf_index+'][dt_emissao]" id="nf_dt_emissao_'+nf_index+'" class="form-control"></td>'
                + '  <td><input name="file_xml['+nf_index+']" id="file_xml_'+nf_index+'" type="file" class="filestyle" data-buttonText="Localizar arquivo" data-input="false" data-icon="false"></td>'
                + '  <td><input name="file_danfe['+nf_index+']" id="file_danfe_'+nf_index+'" type="file" class="filestyle" data-buttonText="Localizar arquivo" data-input="false" data-icon="false"></td>'
                + '  <td>'
                + '    <input type="button" class="btn btn-danger" value="-" id="del_row_'+nf_index+'">'
                + '  </td>'
                + '</tr>';

        $('#table-nf tbody').append(tr);  
        
        $('#del_row_'+nf_index).on("click",function() {
            var tr = $(this).closest('tr');
            tr.css("background-color","#FF3700");
            tr.fadeOut(400, function(){
                tr.remove();
            });
            return false;
        });

        $("#file_xml_"+ nf_index).filestyle({icon: false, input: false, buttonText: "Localizar arquivo"});
        $("#file_danfe_"+ nf_index).filestyle({icon: false, input: false, buttonText: "Localizar arquivo"});
        $("#nf_dt_emissao_"+ nf_index).inputmask({mask: '99/99/9999'});
        $("#nf_nro_"+ nf_index).keydown(function (e) {
			// Allow: backspace, delete, tab, escape, enter and .
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
				(e.keyCode >= 35 && e.keyCode <= 40)) {
					return;
			}
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
    	});


        nf_index++;
    });


    $("#qtde_volumes").keypress(function() {     
        $('.div-datepicker-inline').hide();
        $('#dt_solicitada').val('');
    });

    $("#qtde_volumes").blur(function() {   
        var limite = $(this).val();

        $('.div-datepicker-inline').show();

        $.ajax({
            type: "GET", 
            url: "{{ url('agendamentos/limite-diario') }}/" + limite,    
            dataType: "json", 

            success: function(data) {
                $('#datepicker-inline').datepicker('setDatesDisabled', data.concat(baseDatesDisabled));
            }, 

            error: function(xhr) { 
                swal("Informe a quantidade de volumes.");
                $('.div-datepicker-inline').hide();
                $('#dt_solicitada').val('');
            }
        });

        $("#paga_descarga").change();

    });

            
    $("#paga_descarga").change(function() {     
        if ($(this).val() == '1') 
        {
            var qtde_volumes = ($("#qtde_volumes").val() != '') ? parseInt($("#qtde_volumes").val()) : 0;
            var valor_descarga = {{ trim($valor_descarga) }};
            var total = qtde_volumes * valor_descarga;
            $("#valor_descarga").val( total.toFixed(2) ); 

            $('#valor_descarga').priceFormat({
                prefix: 'R$ ',
                centsSeparator: ',',
                thousandsSeparator: '.',
            });

            $(".div_valor_descarga").show();    
        }
        else
        {
            $(".div_valor_descarga").hide();
        }
    });


@endsection
</script>