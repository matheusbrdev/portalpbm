@extends('layouts.app')

@section('content')

<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Visualizar Solicitação</h4>
    </div>
</div>
<!-- Page-Title -->

    <style>
        .form-control { text-transform: uppercase }
    </style>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">SOLICITAÇÃO</h3>
                </div>
                <div class="panel-body form-horizontal">

                    <style>
                        table { width:80%; padding:7px; }
                        td { padding:8px; line-height: 1.42857; }
                        table { border-collapse: collapse; }
                        table, th, td { border: 1px solid #ccc; }
                        table { font-size:14px }
                    </style>
                    
                    <div id="print">

                        <h5>DETALHES DO PEDIDO</h5>

                        <table>
                            <tr>
                                <td style="width:30%">PEDIDO</td>
                                <td>{{ $solicitacao->nro_pedido }}</td>
                            </tr>
                            <tr>
                                <td>EMISSÃO</td>
                                <td>{{ date_br($solicitacao->dt_emissao) }}</td>
                            </tr>
                            <tr>
                                <td>FORNECEDOR</td>
                                <td>{{ $solicitacao->fornecedor->cpf_cnpj }} - {{ strtoupper($solicitacao->fornecedor->razao_social) }}</td>
                            </tr>      
                            <tr>
                                <td>TRANSPORTADORA</td>
                                <td>{{ $solicitacao->transportadora->cpf_cnpj }} - {{ strtoupper($solicitacao->transportadora->razao_social) }}</td>
                            </tr>                                                                                            
                        </table>

                        <h5 style="margin-top:25px">DETALHES DA SOLICITAÇÃO</h5>

                        <table>
                            <tr>
                                <td style="width:30%">TIPO DE SOLICITAÇÃO</td>
                                <td>{{ @$solicitacao->tipoSolicitacao->nome }}</td>
                            </tr>      
                            <tr>
                                <td>PREVISÃO DE ENTREGA</td>
                                <td>{{ date_br($solicitacao->dt_solicitada) }}</td>
                            </tr>                                                                                        
                            <tr>
                                <td>SITUAÇÃO</td>
                                <td>{{ @$solicitacao->statusAgendamento->nome }}</td>
                            </tr>      
                            <tr>
                                <td>TIPO DE VEICULO</td>
                                <td>{{ @$solicitacao->tipoVeiculo->nome }}</td>
                            </tr> 
                            <tr>
                                <td>TIPO DE PRODUTO</td>
                                <td>{{ @$solicitacao->tipoProduto->nome }}</td>
                            </tr>       
                            <tr>
                                <td>FORMA DE ALOCAÇÃO</td>
                                <td>{{ @$solicitacao->formaAlocacao->nome }}</td>
                            </tr>      
                            <tr>
                                <td>PAGA DESCARGA</td>
                                <td>{{ ($solicitacao->paga_descarga) ? "SIM" : "NÃO" }}</td>
                            </tr>     
                            @if ($solicitacao->paga_descarga)
                            <tr>
                                <td>VALOR DA DESCARGA</td>
                                <td>R$ {{ number_format($solicitacao->qtde_volumes * $solicitacao->valor_descarga_item, 2,",",".") }}</td>
                            </tr> 
                            @endif                                
                            <tr>
                                <td>FORMA DE DEVOLUÇÃO</td>
                                <td>{{ @$solicitacao->formaDevolucao->nome }}</td>
                            </tr>                                                                                                                                                                                                                      
                            <tr>
                                <td>SUGESTÃO / RECLAMAÇÃO</td>
                                <td>{{ @$solicitacao->sugestao }}</td>
                            </tr> 
                        </table>

                        <h5 style="margin-top:25px">AUTORIZAÇÃO</h5>

                        <table>
                            <tr>
                                <td style="width:30%">PROTOCOLO</td>
                                <td>{{ $solicitacao->protocolo }}</td>
                            </tr>
                            <tr>
                                <td>DATA AGENDADA</td>
                                <td>{{ date_br($solicitacao->dt_agendada) }}</td>
                            </tr>     
                            <tr>
                                <td>OBSERVAÇÃO</td>
                                <td>{{ date_br($solicitacao->obs_agendamento) }}</td>
                            </tr>                                                                                                                  
                        </table>

                    </div>

                    <br>

                    <div class="row">
                        <div class="col-md-6">
                            <a id="bt-retornar" href="{{ url('/') }}" class="btn btn-default">Retornar</a>
                            <a id="bt-print" onclick="PrintElem('#print')" href="#" class="btn btn-primary">Imprimir</a>
                            @if ($solicitacao->status_agendamento_id == 1)
                                <a id="bt-cancelar" href="{{ url('agendamentos/cancelar/'.$solicitacao->id) }}" onclick="return confirm('Tem certeza que deseja cancelar esta solicitação?');" class="btn btn-danger">Cancelar Solicitação</a>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>            

@endsection

<script>

@section('jquery.ready')

$('#bt-print').click(function() {
        var mywindow = window.open('', '', 'height=600,width=800');
        mywindow.document.write('<html><head><title>my div</title>');
        mywindow.document.write('<link rel="stylesheet" href="{!! asset('assets/css/bootstrap.min.css') !!}" type="text/css" />');
        mywindow.document.write('<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" type="text/css" />');
        mywindow.document.write('<style> body { margin:15px; } table { font-size:12px } table { width:80%; padding:7px; } </style>');
        mywindow.document.write('<style> td { padding:8px; line-height: 1.42857; } table { border-collapse: collapse; } table, th, td { border: 1px solid #ccc; } </style>');
        mywindow.document.write('<style> h5 { font-weight: bold } </style>');              
                    
        mywindow.document.write('</head><body >');
        mywindow.document.write('<div> <img src="{{ asset('assets/images/logo.png') }}" width="150"> </div> <br>')
        mywindow.document.write($('#print').html());
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
});


@endsection
</script>    