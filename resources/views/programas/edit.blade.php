@extends('layouts.application')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Editando programa...</h4>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">

            {!! Form::model($programa, ['url'=>'admin/programas/'.$programa->id, 'method'=>'PATCH']) !!}

                @include('programas.form')

            {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection