@extends('layouts.application')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Programas  
            <a href="{!! url('admin/programas/create') !!}">
                <i class="md md-2x md-add-circle-outline add-primary"
                    data-toggle="tooltip" data-placement="right" title="" data-original-title="Novo registro"></i> 
            </a>
        </h4>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
 
            <table id="datatable-buttons" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Descrição</th>
                        <th width="30%">Concentrador</th>
                        <th width="10%">Ações</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($programas as $programa)
                    <tr>
                        <td>{!! $programa->descricao !!}</td>
                        <td>{!! $programa->concentrador->descricao !!}</td>
                        <td>
                            {!! Form::open(['method'=>'delete', 'url'=>'admin/programas/'.$programa->id, 'class'=>'form-inline']) !!}
                                <div>
                                    <a href="{!! url('admin/programas/'.$programa->id.'/edit') !!}" class="btn btn-default btn-xs btn-edit">
                                        <i class="fa fa-pencil fa-1"></i>
                                    </a>

                                    <button type="submit" class="btn btn-default btn-xs btn-delete"
                                        onclick="return confirm('Deseja realmente excluir este registro?');">
                                        <i class="fa fa-trash fa-1"></i>
                                    </button>
                                </div>        
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>    
</div>
@endsection
