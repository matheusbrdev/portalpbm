
<div class="row">
    <div class="col-md-6">
        
        <div class="form-group">
            {!! Form::label('descricao', 'Descrição') !!}
            {!! Form::text('descricao', null, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('concentrador_id', 'Concentrador') !!}
            {!! Form::select('concentrador_id', formSelectArray($concentradores), null, ['class'=>'form-control', 'id'=> 'concentrador_id']) !!}
        </div>

        <div class="form-group">
            <input type="submit" value="Salvar" class="btn btn-primary">
            <a href="{{ url('admin/programas') }}" class="btn btn-default">Cancelar</a>
        </div>

    </div>
</div>
