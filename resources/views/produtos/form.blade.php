
<style>
input[readonly], textarea[readonly] {
    cursor: default;
}
</style>

<div class="row">
    <div class="col-md-6">
        
        <div class="form-group">
            {!! Form::label('descricao', 'Descrição') !!}
            {!! Form::text('descricao', null, ['class'=>'form-control', 'readonly']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('programa_id', 'Programa') !!}
            {!! Form::select('programa_id', formSelectArray($programas), null, ['class'=>'form-control', 'id'=> 'programa_id']) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('codigos_barras', 'Cod. Barras') !!}
            {!! Form::textarea('codigos_barras', null, ['class'=>'form-control', 'id'=>'codigos_barras', 'rows'=>5, 'readonly']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('cod_itec', 'Cod. Itec') !!}
            {!! Form::text('cod_itec', null, ['class'=>'form-control', 'id'=>'cod_itec', 'readonly']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('forma_desconto', 'Forma desconto') !!}
            {!! Form::textarea('forma_desconto', null, ['class'=>'form-control', 'id'=>'forma_desconto', 'rows'=>5]) !!}
        </div>

        <div class="form-group">
            <input type="submit" value="Salvar" name="bt_salvar" class="btn btn-primary">
            @if ( ! isset($produto))
            <input type="submit" value="Salvar e continuar" name="bt_continuar" class="btn btn-primary">
            @endif
            <a href="{{ url('admin/produtos') }}" class="btn btn-default">Cancelar</a>
        </div>

    </div>

    <div class="col-md-6">

        <div class="form-group">
            {!! Form::label('orientacoes', 'Orientações') !!}
            {!! Form::textarea('orientacoes', null, ['class'=>'form-control', 'id'=>'orientacoes', 'rows'=>5]) !!}
        </div>

    </div>    
</div>

<script>  
@section('jquery.ready')
    
    $('#cod_itec').onlyNumbers();

    $('input[readonly]').focus(function(){
        this.blur();
    });
    $('textarea[readonly]').focus(function(){
        this.blur();
    });

    tinymce.init({
            selector: "textarea#orientacoes",
            theme: "modern",
            height:255,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste textcolor"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
            style_formats: [
                /*{title: 'Negrito', inline: 'b'},*/
                /*{title: 'Destaque Médio', block: 'h4', styles: {color: '#4c5667;'}},
                {title: 'Destaque Maior', block: 'h1', styles: {color: '#4c5667;'}},*/
                /*{title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},*/
                /*{title: 'Estilo de tabela', classes: 'estilo'}*/
                /*{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}*/
            ]
        });


    $("input[type=radio]").click(function() {
        var selected = $( "input[type=radio]:checked" ).val();

        $( "#q" ).val("");  

        // pesquisa por descrição: habilita o autocomplete   
        if (selected == 'descricao') {
            $( "#q" ).off("keydown"); // dsabilita onlyNumbers()
            $( "#q" ).unbind("keypress"); // desabilita evento keypress()
            $( "#q" ).autocomplete({
                serviceUrl: '{{ url("/admin/itec-autocomplete") }}',
                paramName: 'q',
                dataType: 'json',
                showNoSuggestionNotice: true,
                noSuggestionNotice: 'Desculpe, nenhum resultado encontrado.',
                onSelect: function(suggestion) {
                    //$( "#status" ).html('Selecionado: ' + suggestion.value + ', ' + suggestion.data);
                    produto(suggestion.data);
                }
            });    
        }
        // pesquisa por código de barras ou código itec: desabilita o autocomplete
        else {
            $( "#q" ).autocomplete().clear();
            $( "#q" ).autocomplete().dispose();
            $( "#q" ).onlyNumbers();

            $( '#q' ).keypress(function(e) {
                if(e.which == 13) {
                    produto($( "#q" ).val());
                }
            });
        }

    })


    // inicializa a pesquisa padrão
    $( "input[type=radio]:checked" ).trigger( "click" );


    function produto(id)
    {
        var selected = $( "input[type=radio]:checked" ).val();
        
        var url = "/itec-produto";

        if (selected == 'cod_barras') 
            url = "/itec-produto-por-codbarras";
        else if (selected == 'cod_itec')
            url = "/itec-produto-por-coditec";

        var request = $.ajax({
          url: '{{ url("admin") }}' + url,
          method: "GET",
          data: { id : id },
          dataType: "json",
          beforeSend: function(){
            $('.loader').show();
            $( "#descricao" ).val("");
            $( "#cod_itec" ).val("");
            $('#codigos_barras').empty();            
          },
          complete: function(){
            $('.loader').hide();
          },          
        });
         
        request.done(function( data ) {
            console.log(data);

            if (data.CD_PROD) {
                $( "#descricao" ).val(data.DS_PROD);
                $( "#cod_itec" ).val(data.CD_PROD);
                $( "#codigos_barras").val(data.codigos_barras);

                /*
                $.each(data.codigos_barras, function(index, value) {
                    $('#codigos_barras').append(value);
                });
                */

            } else {
                alert("Produto não encontrado no Itec.");
            }
        });

        

    }


@stop
</script>