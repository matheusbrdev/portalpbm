@extends('layouts.application')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Editando produto...</h4>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">

            {!! Form::model($produto, ['url'=>'admin/produtos/'.$produto->id, 'method'=>'PATCH']) !!}

                @include('produtos.form')

            {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection