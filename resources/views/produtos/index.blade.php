@extends('layouts.application')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Produtos
            <a href="{!! url('admin/produtos/create') !!}">
                <i class="md md-2x md-add-circle-outline add-primary"
                    data-toggle="tooltip" data-placement="right" title="" data-original-title="Novo registro"></i>
            </a>
        </h4>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">

            <table id="datatable-buttons" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Descrição</th>
                        <th width="10%">Cód. Itec</th>
                        <th width="20%">Programa</th>
                        <th width="15%">Atualizado em</th>
                        <th width="8%">Ações</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($produtos as $produto)
                    <tr>
                        <td>{!! $produto->descricao !!}</td>
                        <td>{!! $produto->cod_itec !!}</td>
                        <td>{!! $produto->programa->descricao !!}</td>
                        <td>{{ datetime_br($produto->updated_at)}} </td>
                        <td>
                            {{Form::open(['method'=>'delete', 'url'=>'admin/produtos/'.$produto->id, 'class'=>'form-inline'])}}
                                <div>
                                    <a href="{!! url('admin/produtos/'.$produto->id.'/edit') !!}" class="btn btn-default btn-xs btn-edit">
                                        <i class="fa fa-pencil fa-1"></i>
                                    </a>

                                    <button type="submit" class="btn btn-default btn-xs btn-delete"
                                        onclick="return confirm('Deseja realmente excluir este registro?');">
                                        <i class="fa fa-trash fa-1"></i>
                                    </button>
                                </div>
                            {{ Form::close() }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>
@endsection
