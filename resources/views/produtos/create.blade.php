@extends('layouts.application')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Novo produto...</h4>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Pesquisar produto no Itec</div>

            <div class="panel-body">

                <div class="radio radio-info radio-inline">
                    <input type="radio" id="inlineRadio1" value="descricao" name="t" checked>
                    <label for="inlineRadio1"> Nome do Produto </label>
                </div>

                <div class="radio radio-info radio-inline">
                    <input type="radio" id="inlineRadio2" value="cod_barras" name="t">
                    <label for="inlineRadio2"> Código de Barras </label>
                </div>

                <div class="radio radio-info radio-inline">
                    <input type="radio" id="inlineRadio3" value="cod_itec" name="t">
                    <label for="inlineRadio3"> Código Itec </label>
                </div>

                <div class="form-group" style="margin-top:15px">
                    <input type="text" name="q" id="q" class="form-control" style="background-color: white"> 
                </div>

                <div id="status"></div>

            </div>

        </div>
    </div>
</div>

            {!! Form::open(['url' => 'admin/produtos']) !!}
    
                @include('produtos.form')
                
            {!! Form::close() !!}

        </div>
    </div>
</div>

@endsection

