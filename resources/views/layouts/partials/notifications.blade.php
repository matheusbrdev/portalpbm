@if (count($errors) > 0)
    @foreach ($errors->all() as $error)
        $.Notification.autoHideNotify('error', 'bottom right', '','{!! $error !!}');
    @endforeach
@endif

@if ($message = Session::get('success'))
    @if(is_array($message))
        @foreach ($message as $msg)
            $.Notification.autoHideNotify('success', 'bottom right', '','{!! $msg !!}');
        @endforeach
    @else
        $.Notification.autoHideNotify('success', 'bottom right', '','{!! $message !!}');
    @endif
@endif

@if ($message = Session::get('error'))
    @if(is_array($message))
        @foreach ($message as $msg)
            $.Notification.autoHideNotify('error', 'bottom right', '','{!! $msg !!}');
        @endforeach
    @else
        $.Notification.autoHideNotify('error', 'bottom right', '','{!! $message !!}');
    @endif
@endif

@if ($message = Session::get('warning'))
    @if(is_array($message))
        @foreach ($message as $msg)
            $.Notification.autoHideNotify('warning', 'bottom right', '','{!! $msg !!}');
        @endforeach
    @else
        $.Notification.autoHideNotify('warning', 'bottom right', '','{!! $message !!}');
    @endif
@endif

@if ($message = Session::get('info'))
    @if(is_array($message))
        @foreach ($message as $msg)
            $.Notification.autoHideNotify('info', 'bottom right', '','{!! $msg !!}');
        @endforeach
    @else
        $.Notification.autoHideNotify('info', 'bottom right', '','{!! $message !!}');
    @endif
@endif