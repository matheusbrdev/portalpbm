<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

        <title>Agendamento de Fornecedores - Farmácia Indiana</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        
        <meta content="" name="description">
        <meta content="" name="author">

        <link href="{{ asset('assets-login-2/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets-login-2/bootstrap.min.css') }}" rel="stylesheet" type="text/css">


        <link href="{{ asset('assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css">

        <style>

        </style>
        
        <script>
            var ASSETS_URL = '{!! asset("/") !!}';
            var APP_URL = '{!! url("/") !!}';
        </script>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body>
 
        <!-- BEGIN LOGIN -->
        <div class="content">

            <div style="height:50px"></div>
            @yield('content')
        
        </div>
        <div class="copyright text-center">
            {{ date('Y') }} © Farmácia Indiana.
        </div>

        <!--[if lt IE 9]>
        <script src="assets/global/plugins/respond.min.js"></script>
        <script src="assets/global/plugins/excanvas.min.js"></script>
        <![endif]-->
        <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/detect.js') }}"></script>
        <script src="{{ asset('assets/js/fastclick.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.blockUI.js') }}"></script>
        <script src="{{ asset('assets/js/waves.js') }}"></script>
        <script src="{{ asset('assets/js/wow.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.scrollTo.min.js') }}"></script>

        <script src="{{ asset('assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>

        <script src="{{ asset('assets/js/jquery.maskedinput-1.1.4.pack.js') }}"></script>

        <script>
        jQuery(document).ready(function () {
            /*
            Metronic.init(); // init metronic core components
            Layout.init(); // init current layout
            Login.init();
            Demo.init();
            */
                @section('jquery.ready')

                @show

        });
        </script>

        <!-- END JAVASCRIPTS -->
    
    <!-- END BODY -->

<style>

</style></body></html>