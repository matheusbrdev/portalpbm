<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color:#c7edfc">Produto</div>

                <div class="panel-body">

                    <style type="text/css">

                        .interno > table {
                            border-collapse: collapse;
                        }

                        .interno > table, .interno > table th, .interno > table td {
                            border: 1px solid black;
                        }

                        table.resultado  td { 
                            font-size: 18px; padding: 10px; vertical-align: top; 
                        }

                        table.resultado  th { 
                            font-size: 18px; padding: 10px; vertical-align: top; font-weight: bold; 
                        }

                        .destaque {
                            color: #d24040
                        }

                    </style>

                    <h4>{!! $produto->descricao !!}</h4>
                    <hr>

                    <div class="row">

                        @if ($produto->programa->concentrador->anexo)
                        <div class="col-md-2">
                            <img src="{{ url('uploads/'.$produto->programa->concentrador->anexo) }}" width="120">
                        </div>
                        @endif

                        <div class="col-md-8">

                            <table class="resultado">
                                <tr>
                                    <th>Programa de benefício:</th>
                                    <td class="destaque">{!! $produto->programa->descricao !!}</td>
                                </tr>
                                <tr>
                                    <th>Concentrador:</th>
                                    <td class="destaque">{!! $produto->programa->concentrador->descricao !!} </td>
                                </tr>  
                                <tr>
                                    <th>Forma de desconto:</th>
                                    <td class="destaque">{!! $produto->forma_desconto !!}</td>
                                </tr>    
                                <tr>
                                    <th>Orientações do programa/paciente:</th>
                                    <td class="interno class="destaque"">{!! $produto->orientacoes !!} </td>
                                </tr>                                                                      
                            </table>

                        </div>
                    </div>



                </div>

            </div>
        </div>
    </div>
</div>